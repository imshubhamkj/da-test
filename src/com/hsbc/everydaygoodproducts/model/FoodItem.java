package com.hsbc.everydaygoodproducts.model;

public final class FoodItem extends Product {
	private String dateOfManufacture;
	private String dateOfExpiry;
	private boolean isVegitarian;

	public FoodItem(String itemName, double unitPrice, long quantity, String dateOfManufacture, String dateOfExpiry,
			boolean isVegitarian) {
		super(itemName, unitPrice, quantity);
		this.dateOfManufacture = dateOfManufacture;
		this.dateOfExpiry = dateOfExpiry;
		this.isVegitarian = isVegitarian;
	}

	public String getDateOfManufacture() {
		return dateOfManufacture;
	}

	public void setDateOfManufacture(String dateOfManufacture) {
		this.dateOfManufacture = dateOfManufacture;
	}

	public String getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(String dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public boolean isVegitarian() {
		return isVegitarian;
	}

	public void setVegitarian(boolean isVegitarian) {
		this.isVegitarian = isVegitarian;
	}

}
