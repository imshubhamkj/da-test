package com.hsbc.everydaygoodproducts.model;

public class Product {
	private long itemCode;
	private String itemName;
	private double unitPrice;
	private long quantity;
	private static long counterId = 1000;
	private long quantitySold;
	public Product(String itemName, double unitPrice, long quantity) {
		this.itemName = itemName;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
		this.itemCode = counterId++;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public long getQuantity() {
		return quantity;
	}
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	public long getItemCode() {
		return itemCode;
	}
	public long getQuantitySold() {
		return quantitySold;
	}
	public void setQuantitySold(long quantitySold) {
		this.quantitySold = quantitySold;
	}
	@Override
	public String toString() {
		return "Product [itemCode=" + itemCode + ", itemName=" + itemName + ", unitPrice=" + unitPrice + ", quantity="
				+ quantity + ", quantitySold=" + quantitySold + "]";
	}
	
	
	
}
