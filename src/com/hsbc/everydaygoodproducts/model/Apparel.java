package com.hsbc.everydaygoodproducts.model;

public final class Apparel extends Product {

	private double sizeInCm;
	private String material;

	public Apparel(String itemName, double unitPrice, long quantity, double sizeInCm, String material) {
		super(itemName, unitPrice, quantity);
		this.sizeInCm = sizeInCm;
		this.material = material;
	}

	public double getSizeInCm() {
		return sizeInCm;
	}

	public void setSizeInCm(double sizeInCm) {
		this.sizeInCm = sizeInCm;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

}
