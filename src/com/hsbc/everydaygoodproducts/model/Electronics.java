package com.hsbc.everydaygoodproducts.model;

public final class Electronics extends Product {
	private int warrentyInMonths;

	public Electronics(String itemName, double unitPrice, long quantity, int warrentyInMonths) {
		super(itemName, unitPrice, quantity);
		this.warrentyInMonths = warrentyInMonths;
	}

	public int getWarrentyInMonths() {
		return warrentyInMonths;
	}

	public void setWarrentyInMonths(int warrentyInMonths) {
		this.warrentyInMonths = warrentyInMonths;
	}

}
