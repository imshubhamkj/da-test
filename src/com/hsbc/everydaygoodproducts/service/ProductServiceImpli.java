package com.hsbc.everydaygoodproducts.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import com.hsbc.everydaygoodproducts.dao.ArrayListProductDAOImpli;
import com.hsbc.everydaygoodproducts.dao.ProductDAO;
import com.hsbc.everydaygoodproducts.exception.ItemNotFoundException;
import com.hsbc.everydaygoodproducts.model.Apparel;
import com.hsbc.everydaygoodproducts.model.Electronics;
import com.hsbc.everydaygoodproducts.model.FoodItem;
import com.hsbc.everydaygoodproducts.model.Product;

public final class ProductServiceImpli implements ProductService {
	
	private ProductDAO dao = new ArrayListProductDAOImpli();

	@Override
	public Product createProduct(String itemName, double unitPrice, long quantity, double sizeInCm, String material) {
		// TODO Auto-generated method stub
		Apparel product = new Apparel(itemName, unitPrice, quantity, sizeInCm, material);
		return this.dao.saveProduct(product);
		
	}

	@Override
	public Product createProduct(String itemName, double unitPrice, long quantity, int warrentyInMonths) {
		// TODO Auto-generated method stub
		Electronics product = new Electronics(itemName, unitPrice, quantity, warrentyInMonths);
		return this.dao.saveProduct(product);
	}

	@Override
	public Product createProduct(String itemName, double unitPrice, long quantity, String dateOfManufacture,
			String dateOfExpiry, boolean isVegitarian) {
		// TODO Auto-generated method stub
		FoodItem product = new FoodItem(itemName, unitPrice, quantity, dateOfManufacture, dateOfExpiry, isVegitarian);
		return product;
	}

	@Override
	public Collection<Product> fetchAllProducts() {
		// TODO Auto-generated method stub
		return this.dao.fetchAllProducts();
	}

	@Override
	public void deleteProduct(long itemCode) {
		// TODO Auto-generated method stub
		this.dao.deleteProduct(itemCode);
		
	}

	@Override
	public Product fetchProductByItemCode(long itemCode) throws ItemNotFoundException {
		// TODO Auto-generated method stub
		return this.dao.fetchProductByItemCode(itemCode);
	}

	@Override
	public Collection<Product> createReport(String category) throws ItemNotFoundException {
		// TODO Auto-generated method stub
		List<Product> topThreeProducts = new ArrayList<>();
		Collection<Product> products = this.dao.fetchAllProducts();
		Iterator< Product > itr = products.iterator();
		if(category.equals("Food")) {
			ArrayList<Product> foods = new ArrayList<>();
			
			while(itr.hasNext()) {
				Product product = itr.next();
				if (product instanceof FoodItem) {
					FoodItem foodProduct = (FoodItem) product;
					foods.add(foodProduct);
				}
			}
			
			foods.sort(new sortOnSoldQuantity());
			return foods;
						
		}
		
		if(category.equals("appral")) {
			ArrayList<Product> apprals = new ArrayList<>();
			
			while(itr.hasNext()) {
				Product product = itr.next();
				if (product instanceof Apparel) {
					Apparel appralProduct = (Apparel) product;
					apprals.add(appralProduct);
				}
			}
			
			apprals.sort(new sortOnSoldQuantity());
			return apprals;
						
		}
		if(category.equals("electronics")) {
			ArrayList<Product> electronics = new ArrayList<>();
			
			while(itr.hasNext()) {
				Product product = itr.next();
				if (product instanceof Electronics) {
					Electronics electronicsProduct = (Electronics) product;
					electronics.add(electronicsProduct);
				}
			}
			
			electronics.sort(new sortOnSoldQuantity());
			return electronics;
						
		}
		
		throw new ItemNotFoundException("Item not found");
	}

	@Override
	public Product buyProduct(long itemCode,long quantity) throws ItemNotFoundException {
		// TODO Auto-generated method stub
		Product product  = this.dao.fetchProductByItemCode(itemCode);
		if(product.getQuantity()<quantity) {
		product.setQuantitySold(product.getQuantitySold()+quantity);
		product.setQuantity(product.getQuantity()-quantity);
		return product;
		}
		throw new ItemNotFoundException("Item not found");
	}

}


class sortOnSoldQuantity implements Comparator<Product>{

	@Override
	public int compare(Product arg0, Product arg1) {
		// TODO Auto-generated method stub
		return (-1) * (int)(arg0.getQuantitySold() - arg1.getQuantitySold());
	}
	
}
