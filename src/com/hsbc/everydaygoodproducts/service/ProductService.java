package com.hsbc.everydaygoodproducts.service;

import java.util.Collection;

import com.hsbc.everydaygoodproducts.exception.ItemNotFoundException;
import com.hsbc.everydaygoodproducts.model.Product;

public interface ProductService {
	
	Product createProduct(String itemName, double unitPrice, long quantity, double sizeInCm, String material);
	Product createProduct(String itemName, double unitPrice, long quantity, int warrentyInMonths);
	Product createProduct(String itemName, double unitPrice, long quantity, String dateOfManufacture, String dateOfExpiry,boolean isVegitarian);
	Collection<Product> fetchAllProducts();
	void deleteProduct(long itemCode);
	Product fetchProductByItemCode(long itemCode) throws ItemNotFoundException;
	Collection<Product> createReport(String category) throws ItemNotFoundException;
	Product buyProduct(long itemCode,long quantity) throws ItemNotFoundException;

}
