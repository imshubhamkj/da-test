package com.hsbc.everydaygoodproducts.client;

import java.util.Scanner;

import com.hsbc.everydaygoodproducts.service.ProductService;
import com.hsbc.everydaygoodproducts.service.ProductServiceImpli;

/**
 * 
 * @author shubham kumar
 *
 */
public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ProductService service = new ProductServiceImpli();
		Scanner in = new Scanner(System.in);
		String choice = in.next();
		in.close();
		
		
		service.createProduct("tv", 3000, 100, 12);
		service.createProduct("Mobile", 8000, 97, 24);
		service.createProduct("watch", 1500, 57, 18);
		
		
		service.createProduct("Tshirt", 500, 50, 50, "cotton");
		System.out.println("executed");
		
		

	}

}
