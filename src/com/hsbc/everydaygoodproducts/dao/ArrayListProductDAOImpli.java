package com.hsbc.everydaygoodproducts.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.hsbc.everydaygoodproducts.exception.ItemNotFoundException;
import com.hsbc.everydaygoodproducts.model.Product;

public final class ArrayListProductDAOImpli implements ProductDAO {
	
	private List<Product> products = new ArrayList<>();

	@Override
	public Product saveProduct(Product product) {
		// TODO Auto-generated method stub
		products.add(product);
		return product;
	}

	@Override
	public Collection<Product> fetchAllProducts() {
		// TODO Auto-generated method stub
		return products;
	}

	@Override
	public void deleteProduct(long itemCode) {
		// TODO Auto-generated method stub
		for(Product product : products) {
			if(product.getItemCode() == itemCode) {
				products.remove(product);
			}
		}
		
	}

	@Override
	public Product fetchProductByItemCode(long itemCode) throws ItemNotFoundException {
		// TODO Auto-generated method stub
		for(Product product : products) {
			if(product.getItemCode() == itemCode) {
				return product;
			}
		}
		throw new ItemNotFoundException("Item not found");
	}

	@Override
	public Product update(Product product) throws ItemNotFoundException {
		// TODO Auto-generated method stub
		long itemCode = product.getItemCode();
		for(Product productInList : products) {
			if(product.getItemCode() == itemCode) {
				productInList = product;
				return product;
			}
		}
		throw new ItemNotFoundException("Item not found");
	}

}
