package com.hsbc.everydaygoodproducts.dao;

import java.util.Collection;

import com.hsbc.everydaygoodproducts.exception.ItemNotFoundException;
import com.hsbc.everydaygoodproducts.model.Product;

public interface ProductDAO {
	
	Product saveProduct(Product product);
	Collection<Product> fetchAllProducts();
	void deleteProduct(long itemCode);
	Product fetchProductByItemCode(long itemCode) throws ItemNotFoundException;
	Product update(Product product) throws ItemNotFoundException;

}
